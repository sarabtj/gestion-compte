package com.formation.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.formation.entities.Compte;
import com.formation.entities.Personne;
import com.formation.services.CompteService;
import com.formation.services.imp.CompteServiceImp;

@ManagedBean
@ViewScoped
public class CompteBean {

	@ManagedProperty(value="#{cptServ}")
	private CompteService cptServ;

	private Personne pers = new Personne();

	private List<Compte> list = new ArrayList<Compte>();

	private Compte cpt = new Compte();
	
	private boolean btnAdd=true;
	private boolean btnEdit=false;
	
	public void clickEdit()
	{
		btnAdd=false;
		btnEdit=true;
		pers=cpt.getPersonne();
		
	}
	
	public void clickAdd()
	{
		btnAdd=true;
		btnEdit=false;
		pers=new Personne();
		cpt=new Compte();
		
	}
	
	public void modifier()
	{
		
		try {
			cpt.setPersonne(pers);
			cptServ.update(cpt);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
		public void ajouter()
	{
		try {
			cpt.setPersonne(pers);
			cptServ.save(cpt);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Ajout effectuee."));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erruer d'ajout !!"));
		}
	}
	
	public void supprimer()
	{
		try {
			cptServ.delete(cpt);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Compte getCpt() {
		return cpt;
	}

	public void setCpt(Compte cpt) {
		this.cpt = cpt;
	}

	public List<Compte> getList() {

		try {

			list = cptServ.findAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	public void setList(List<Compte> list) {
		this.list = list;
	}

	public CompteService getCptServ() {
		return cptServ;
	}

	public void setCptServ(CompteService cptServ) {
		this.cptServ = cptServ;
	}

	public Personne getPers() {
		return pers;
	}

	public void setPers(Personne pers) {
		this.pers = pers;
	}
	
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}



}
