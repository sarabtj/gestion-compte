package com.formation.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.formation.entities.Personne;
import com.formation.services.PersonneService;
import com.formation.services.imp.PersonneServiceImp;

@ManagedBean
@ViewScoped
public class PersonneBean {

	private Personne pers = new Personne();
	@ManagedProperty(value="#{persServ}")
	private PersonneService persServ;
	private List<Personne> list = new ArrayList<Personne>();

	private boolean btnAdd = true, btnEdit = false, read = false;

	public void clickEdit() {
		btnAdd = false;
		btnEdit = true;
		read = true;
	}

	 public void modifier()
	 {
		 try {
			persServ.update(pers);
			annuler();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	
	public PersonneService getPersServ() {
		return persServ;
	}

	public void setPersServ(PersonneService persServ) {
		this.persServ = persServ;
	}

	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public void ajouter() {

		try {
			persServ.save(pers);
			pers = new Personne();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void supprimer() {
		try {
			persServ.delete(pers);
			pers = new Personne();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void annuler() {
		pers = new Personne();
		btnAdd = true;
		btnEdit = false;
		read = false;
	}

	public Personne getPers() {
		return pers;
	}

	public void setPers(Personne pers) {
		this.pers = pers;
	}

	public List<Personne> getList() {

		try {
			list = persServ.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public void setList(List<Personne> list) {
		this.list = list;
	}

}
