package com.formation.dao;

import java.util.List;

import org.hibernate.criterion.Criterion;

public interface GenericDao {
	
	
	public void saveOrUpdate(Object obj) throws Exception;
	public void delete(Object obj) throws Exception;
	List findAll(Class cls) throws Exception;
	public List findByCriterier(Class cls , Criterion crit) throws Exception;
	public List findByCriterier(Class cls , Criterion crit , Criterion crit1) throws Exception;

}
