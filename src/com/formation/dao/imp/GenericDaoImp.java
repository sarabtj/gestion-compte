package com.formation.dao.imp;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;

import com.formation.dao.GenericDao;
import com.formation.utils.HibernateUtil;

public class GenericDaoImp implements GenericDao{
	
	
	protected Session hibernateSession;
	private Transaction tx;
	
	protected void startOperation()
	{
		hibernateSession = HibernateUtil.getInstance().getSession();
		tx=hibernateSession.beginTransaction();
		
	}

	protected void endOperation()
	{
		tx.commit();
		hibernateSession.close();
	}
	@Override
	public void saveOrUpdate(Object obj) {
		startOperation();
		hibernateSession.saveOrUpdate(obj);
		endOperation();	
	}

	@Override
	public void delete(Object obj) {
		startOperation();
		hibernateSession.delete(obj);
		endOperation();		
		
	}

	@Override
	public List findAll(Class cls) {
		startOperation();
		List list=hibernateSession.createCriteria(cls).list();
		endOperation();
		return list;
	}

	@Override
	public List findByCriterier(Class cls, Criterion crit) {
		
		startOperation();
		List list=hibernateSession.createCriteria(cls).add(crit).list();
		endOperation();
		return list;
	}

	@Override
	public List findByCriterier(Class cls, Criterion crit, Criterion crit1) {
		
		startOperation();
		List list=hibernateSession.createCriteria(cls).add(crit).add(crit1).list();
		endOperation();
		return list;
	}

}
