package com.formation.services.imp;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.formation.dao.CompteDao;
import com.formation.dao.imp.CompteDaoImp;
import com.formation.entities.Compte;
import com.formation.entities.Personne;
import com.formation.services.CompteService;

public class CompteServiceImp implements CompteService{
	private CompteDao cmptDao;
	@Override
	public String save(Compte cmpt) throws Exception {
		
		
			cmptDao.saveOrUpdate(cmpt);	
			return "Compte ajouter";
			

		
	}

	@Override
	public String update(Compte cmpt) throws Exception {
		
			cmptDao.saveOrUpdate(cmpt);	
			
			return "Compte ajoutÚsts";
			
	}

	@Override
	public void delete(Compte cmpt) throws Exception {
		
		cmptDao.delete(cmpt);
	}

	@Override
	public List findAll() throws Exception {
		return cmptDao.findAll(Compte.class);
	}

	public CompteDao getCmptDao() {
		return cmptDao;
	}

	public void setCmptDao(CompteDao cmptDao) {
		this.cmptDao = cmptDao;
	}

}
