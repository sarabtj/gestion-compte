package com.formation.services.imp;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.formation.dao.PersonneDao;
import com.formation.dao.imp.PersonneDaoImp;
import com.formation.entities.Personne;
import com.formation.services.PersonneService;
import com.sun.org.apache.regexp.internal.recompile;

public class PersonneServiceImp implements PersonneService{
	private PersonneDao persDao;

	@Override
	public String save(Personne pers) throws Exception {
		Criterion crit=Restrictions.idEq(pers.getCin());
		List<Personne> list=persDao.findByCriterier(Personne.class, crit);
		if(!list.isEmpty())
		{
			return "cin existant";
		}
		
		crit=Restrictions.eq("email", pers.getEmail());
		list=persDao.findByCriterier(Personne.class, crit);
		if(!list.isEmpty())
		{
			return "Email existant";
			
		}
		
		persDao.saveOrUpdate(pers);
		
		return "Personne ajout� avec succ�";
	}

	@Override
	public String update(Personne pers) throws Exception {
		
		Criterion crit=Restrictions.idEq(pers.getCin());
		Criterion crit1=Restrictions.eq("email", pers.getEmail());
		List<Personne> list=persDao.findByCriterier(Personne.class, crit ,crit1);
		if(!list.isEmpty())
		{
			persDao.saveOrUpdate(pers);
		}
		
		
		list=persDao.findByCriterier(Personne.class, crit1);
		if(!list.isEmpty())
		{
			return "Email existant";
			
		}
		
		persDao.saveOrUpdate(pers);
		
		return "Modification effectu� avec succ� avec succ�";
	}

	@Override
	public void delete(Personne pers) throws Exception {
		
		persDao.delete(pers);
	}

	@Override
	public List<Personne> findAll() throws Exception {
		
		return persDao.findAll(Personne.class);
	}

	public PersonneDao getPersDao() {
		return persDao;
	}

	public void setPersDao(PersonneDao persDao) {
		this.persDao = persDao;
	}

}
